FROM scratch
COPY aws-mfa /usr/bin/aws-mfa
ENTRYPOINT ["/usr/bin/aws-mfa"]
